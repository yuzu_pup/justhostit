import {FileRecord} from "./FileRecord";
import {IDatabaseAdaptor} from "./adaptors/IDatabaseAdaptor";

export class Database {
    private _Adaptor : IDatabaseAdaptor;

    constructor(adaptor : IDatabaseAdaptor) {
        this._Adaptor = adaptor;
    }

    public CreateFileRecord(rec : FileRecord) : string {
        return this._Adaptor.Insert("files", rec);

    }

    public GetFileRecord(id : string) : FileRecord {
        let rec = this._Adaptor.Get("files", id) as FileRecord;
        rec.ID = id;
        return rec;
    }
}