export interface IDatabaseAdaptor {
    Insert(table: string, data: any) : string;
    Get(table:string, id: string) : any;
}