import {IDatabaseAdaptor} from "./IDatabaseAdaptor";
import uuid = require('uuid/v4');

export class InMemoryAdaptor implements IDatabaseAdaptor {
    private db : any = {};

    Insert(table: string, data: any) : string {
        if(this.db[table] == null) {
            this.db[table] = {};
        }

        let id = uuid();
        this.db[table][id] = data;

        console.log(`Created record ${id}: ${JSON.stringify(data)}`);

        return id;
    }

    Get(table: string, id: string) {
        if(this.db[table] == null) {
            return null;
        }

        return this.db[table][id];
    }
}