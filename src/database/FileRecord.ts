export class FileRecord {
    public ID : string;
    public Filename : string;
    public UserID : string;
    public TypeHint : string;
}