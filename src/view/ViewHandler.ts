import path = require('path');
import express = require('express');

import {Database} from "../database/Database";
import {FileRecord} from "../database/FileRecord";

export class ViewHandler {
    private _app : express.Application;
    private _db : Database;

    constructor(app : express.Application, db : Database) {
        this._app = app;
        this._db = db;

        this.RegisterRoutes(app);
    }

    private RegisterRoutes(app : express.Application) {
        app.get("/view/:id", (req, res) => {
            let rec : FileRecord = this._db.GetFileRecord(req.params["id"]);

            if(rec != null) {
                res.send(`<html><img src='/uploads/${rec.Filename}'>'</html>`);
            }
        });
    }
}