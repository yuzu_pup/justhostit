import express = require('express');
import multer = require('multer');
import {Database} from "../database/Database";
import process = require('process');
import path = require('path');
import fs = require('fs');

export class UploadHandler {
    private _upload : multer.Instance;
    private _db : Database;

    constructor(app : express.Application, db : Database) {
        this._db = db;

        this._upload = UploadHandler.CreateMulterStorage();
        this.RegisterRoutes(app);
    }

    private static CreateMulterStorage() : multer.Instance {
        let _saveDir = path.join(process.cwd(), "public", "uploads");

        if(fs.existsSync(_saveDir) == false) {
            // TODO: recursive isn't working?
            fs.mkdirSync(_saveDir, { recursive: true });
        }

        let storage = multer.diskStorage({
            destination: (req, file, callback) => {
                callback(null, _saveDir);
            },
            filename: (req, file, callback) => {
                callback(null, `${Date.now()}-${file.originalname}`);
            }
        });

        return multer({storage: storage});
    }

    private RegisterRoutes(app : express.Application) {
        app.post("/upload", this._upload.single("file"), (req, res) => {
            let fileInfo : Express.Multer.File = req.file;

            let id = this._db.CreateFileRecord({
                ID: "",
                Filename: fileInfo.filename,
                UserID: "",
                TypeHint: ""
            });

            res.send(`success: ${id}`);
        });
    }
}
