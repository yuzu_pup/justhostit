import express = require("express");
import {UploadHandler} from "./upload/UploadHandler";
import {Database} from "./database/Database";
import {InMemoryAdaptor} from "./database/adaptors/InMemoryAdaptor";
import {ViewHandler} from "./view/ViewHandler";

let app = express();
const port = 3000;

let db = new Database(new InMemoryAdaptor());
let uploadHandler : UploadHandler = new UploadHandler(app, db);
let viewHandler : ViewHandler = new ViewHandler(app, db);

app.use(express.static("public"))

app.get("/", (req, res) => res.send("Ohai"));


for(let routeObj of app._router.stack) {
    if(routeObj.route != null && routeObj.route.path != null) {
        console.log(routeObj.route.path);
    }
}
app.listen(port, () => { console.log(`listening on ${port}`)});